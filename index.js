const express = require('express');
var bodyParser = require('body-parser');

const userController = require('./controllers/user.controller');

const app = express();
app.use(bodyParser.json({ limit: '10mb' }));
app.use(
  bodyParser.urlencoded({ limit: '10mb', extended: true, parameterLimit: 500 })
);

const port = 3000;

app.post('/user/login', userController.login);
app.post('/user/register', userController.registerUser);
app.delete('/user/', userController.delete);

// listen app
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

module.exports = app;

const fs = require('fs');
const path = require('path');
const pathUsers = path.join(__dirname, '../users.csv');

exports.registerUser = (req, res) => {
  const body = req.body;
  const users = csvReaderUsers();
  const findUserByEmail = users.filter((u) => {
    return u.email == body.email;
  });
  if (findUserByEmail.length) throw new Error('duplicate user!');
  addUser(body.fName, body.lName, body.password, body.email);
  res.send('ok');
};

exports.login = (req, res) => {
  const body = req.body;
  const users = csvReaderUsers();

  const findUser = users.filter((user) => {
    return user.password == body.password && user.email == body.username;
  });
  if (findUser[0]) res.send('ok');
  else res.send('not ok');
};

exports.delete = (req, res) => {
  const body = req.body;
  console.log({ body });
  const users = csvReaderUsers();

  const userExist = users.filter((u) => {
    return u.email == body.email;
  });
  if (!userExist.length) throw new Error('user not exists.');
  const usersWithoutDeleted = users.filter((u) => {
    return u.email != body.email;
  });

  let result = '';

  for (let user of usersWithoutDeleted) {
    result +=
      user.fName +
      ',' +
      user.lName +
      ',' +
      user.password +
      ',' +
      user.email +
      '\n';
  }

  fs.writeFileSync(pathUsers, result);

  res.send('user deleted.');
};

//Functions

function addUser(fName, lName, password, email, path = pathUsers) {
  fs.appendFileSync(
    path,
    '\n' + fName + ',' + lName + ',' + password + ',' + email
  );
}

function csvReaderUsers(path = pathUsers) {
  const file = fs.readFileSync(path, {
    encoding: 'utf-8',
  });

  const lines = file.split('\n');

  const colomns = [];

  for (const item of lines) {
    const data = item.split(',');
    colomns.push({
      fName: data[0],
      lName: data[1],
      password: data[2],
      email: data[3],
    });
  }

  return colomns;
}
